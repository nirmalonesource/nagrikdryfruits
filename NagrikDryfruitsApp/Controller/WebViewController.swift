//
//  WebViewController.swift
//  1StopShopApp
//
//  Created by My Mac on 09/06/20.
//  Copyright © 2020 My Mac. All rights reserved.
//

import UIKit
import WebKit

class WebViewController: UIViewController {

    @IBOutlet var lbltitle: UILabel!
    @IBOutlet var webvw: WKWebView!
    @IBOutlet var textvw: UITextView!
    var str = String()
    var isfrom = String()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        if isfrom == "terms" {
            str = "1) We currently offer same day delivery. \n\n 2) Order place after 5 PM , we will deliver next day but we will try to delivery on same day.\n\n 3) Free delivery for order worth Rs. 500 & Above"
            lbltitle.text = "Terms & Conditions"
        }
        else
        {
            str = "Contact Us  At - \n\n\n Shop No. 42/43,\n Munshi Estate CHS,\n S.L. Road,\n Mulund (W),\n Mumbai  \n\n\n Ashish Vora : 9322847214 / 2593 4060"
            
//            \n\n Mobile: 8108671885 \n\n\n Santosh Sangle/Chandrakant Sangle \n\n\n Address \n\n Ground floor,\n Bldg no 34 \n Boyce Bungalow \n Opp Jakson Bank \n Next to BMC BLOG \n Seleter road Grantroad west \n MUMBAI 400007"
            
            lbltitle.text = "Support"
        }
        

       // webvw.loadHTMLString(str.replacingOccurrences(of: "\n", with: "<br/>"), baseURL: nil)
        
     //   webvw.loadHTMLString(str, baseURL: nil)
        
//        let url = URL(string: "https://www.facebook.com/")
//        webvw.load(URLRequest(url: url!))
        
        textvw.text = str
    }
    

    // MARK: - Button Action
    
    @IBAction func back_click(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }

}
