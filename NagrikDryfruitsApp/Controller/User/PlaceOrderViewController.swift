//
//  PlaceOrderViewController.swift
//  SantoshSangal
//
//  Created by My Mac on 25/06/20.
//  Copyright © 2020 My Mac. All rights reserved.
//

import UIKit
import IBAnimatable
import iOSDropDown
import M13Checkbox

class PlaceOrderViewController: UIViewController {

    @IBOutlet var lblsubtotal: UILabel!
       @IBOutlet var lblDC: UILabel!
       @IBOutlet var lbltotal: UILabel!
  //  @IBOutlet var txtzipcode: AnimatableTextField!
    @IBOutlet var txtzipcode: DropDown!
    @IBOutlet var textviewaddress: AnimatableTextView!
    
    @IBOutlet var chkselfpickup: M13Checkbox!
    @IBOutlet var chkbysantosh: M13Checkbox!
    @IBOutlet var lblmsg: UILabel!
    
    var productlist = [[String:Any]]()
      var dc = Double()
     var subtotal = Double()
     var total = Double()
    
    var lblsubtotalvalue = String()
     var lbldcvalue = String()
     var lbltotalvalue = String()
     var ziparr = [String]()
    var strradio = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        let chkarr = [chkselfpickup,chkbysantosh]
        
        for chk in chkarr {
            chk?.markType = .radio
            chk?.boxType = .circle
            chk?.tintColor = #colorLiteral(red: 0.7854140228, green: 0.182675971, blue: 0.153251963, alpha: 1)
            chk?.secondaryTintColor = UIColor.lightGray
            chk?.stateChangeAnimation = .fill
           // chk?.secondaryCheckmarkTintColor = UIColor.red
        }
        
         chkbysantosh?.checkState = .checked
        strradio = "Delivery By Nagrik Dryfruits"
       lblmsg.text = "1) We currently offer same day delivery. \n\n 2) Order place after 5 PM , we will deliver next day but we will try to delivery on same day.\n\n 3) Free delivery for order worth Rs. 500 & Above"
        
        
        ZipCodeApi()
       // ziparr = ["400001","400002","400004","400006","400007","400008","400018", "4000019","400020","400026","400034","400036", "4000078","4000080","4000088"]
               
               
        
        lblsubtotal.text = lblsubtotalvalue
        lblDC.text = lbldcvalue
        lbltotal.text = lbltotalvalue
        
        txtzipcode.text = UserDefaults.standard.getUserDict()["zipcode"] as? String ?? ""
        textviewaddress.text = UserDefaults.standard.getUserDict()["address"] as? String ?? ""
    }
    

    // MARK: - Button Action
    
    @IBAction func chkself_click(_ sender: M13Checkbox) {
        chkselfpickup.checkState = .checked
        chkbysantosh.checkState = .unchecked
        strradio = "Self Pickup"
         lblmsg.text = "Shop No. 42/43,\n Munshi Estate CHS,\n S.L. Road,\n Mulund (W),\n Mumbai"
    }
    @IBAction func chkbysantosh_click(_ sender: M13Checkbox) {
        chkselfpickup.checkState = .unchecked
        chkbysantosh.checkState = .checked
         strradio = "Delivery By Nagrik Dryfruits"
              lblmsg.text = "1) We currently offer same day delivery. \n\n 2) Order place after 5 PM , we will deliver next day but we will try to delivery on same day.\n\n 3) Free delivery for order worth Rs. 500 & Above"
    }
    
    @IBAction func back_click(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func confirm_click(_ sender: UIButton) {
         ProductOrderApi()
    }
    
     // MARK: - API
    
    func ZipCodeApi()
                                  
            {
                if !isInternetAvailable(){
                    noInternetConnectionAlert(uiview: self)
                }
                else
                {
                    let parameters = [ :
                        ] as [String : Any]
                   let url = ServiceList.SERVICE_URL+ServiceList.ZIPCODE_API
                     callApi(url,
                                   method: .get,
                                   param: parameters, withLoader: true)
                           {
                            (result) in
                                print("LOGINRESPONSE:",result)
                                if result.getBool(key: "status")
                                {
                                 let arr = result.getArrayofDictionary(key: "data")
                                    self.ziparr = arr.compactMap {"\($0["Zipcode"] as? String ?? "0")"}
                                    self.txtzipcode.optionArray = self.ziparr
                                }
                             else
                                {
                                 showToast(uiview: self, msg: result.getString(key: "message"))
                             }
                    }
                }
                
            }
                 
                   func ProductOrderApi()
                                          {
                                              if !isInternetAvailable(){
                                                  noInternetConnectionAlert(uiview: self)
                                              }
                                              else
                                              {
                                                 let url = ServiceList.SERVICE_URL+ServiceList.PLACE_ORDER_API
                                                
                                                var productarr = [[String:Any]]()
                                                var json = String()
                                                for dict in productlist {
                                                    if dict["isPack"] as! String == "1" {
                                                        var singleproduct1 = [
                                                            
                                                            "CategoryID":dict["CategoryID"] ?? "",
                                                            "CategoryName":dict["CategoryName"] ?? "",
                                                            "PKTSize":dict["PKTSize"] ?? "",
                                                            "Price":dict["Price"] ?? "",
                                                            "ProductCode":dict["ProductCode"] ?? "",
                                                            "ProductID":dict["ProductID"] ?? "",
                                                            "ProductImage":dict["ProductImage"] ?? "",
                                                            "ProductName":dict["ProductName"] ?? ""
                                                            
                                                        ]
                                                        
                                                        let singleproduct2 = [
                                                            
                                                            "SubcategoryID":dict["SubcategoryID"] ?? "",
                                                            "SubcategoryName":dict["SubcategoryName"] ?? "",
                                                            "UOMDesc":dict["UOMDesc"] ?? "",
                                                            "UOMID":dict["UOMID"] ?? "",
                                                            "fromCart":false,
                                                            "isAvailable":dict["isAvailable"] ?? "",
                                                            "isPack":dict["isPack"] ?? "",
                                                            "qty":dict["qty"] ?? "",
                                                            "UOMQty": "",
                                                        ]
                                                        
                                                        singleproduct1.merge(dict: singleproduct2)
                                                        
                                                        productarr.append(singleproduct1)
                                                    }
                                                    else
                                                    {
                                                        let Details = dict["Details"] as? [[String:Any]]
                                                        var uid = String()
                                                        var price = String()
                                                        if Details?.count ?? 0 > 0 {
                                                              
                                                            for i in Details ?? []  {
                                                               let j = i["pktsize"] as! String
                                                               // let arr = j.components(separatedBy: " ")
                                                                
                                                                let k = dict["qty"] as? String ?? ""
                                                                let arr1 = k.components(separatedBy: " ")
                                                                if j == arr1[0] {
                                                                    uid = (i["uid"] as? String)!
                                                                      price = (i["price"] as? String)!
                                                                }
                                                                
                                                            }
                                                        }
                                                        
                                                        var singleproduct1 = [
                                                            
                                                            "CategoryID":dict["CategoryID"] ?? "",
                                                            "CategoryName":dict["CategoryName"] ?? "",
                                                            "PKTSize":dict["PKTSize"] ?? "",
                                                            "Price":dict["Price"] ?? "",
                                                            "ProductCode":dict["ProductCode"] ?? "",
                                                            "ProductID":dict["ProductID"] ?? "",
                                                            "ProductImage":dict["ProductImage"] ?? "",
                                                            "ProductName":dict["ProductName"] ?? "",
                                                            "LPPrice":String(price),
                                                            "LPID":uid
                                                            
                                                        ]
                                                        
                                                        

                                                        
                                                        let singleproduct2 = [
                                                            
                                                            "SubcategoryID":dict["SubcategoryID"] ?? "",
                                                            "SubcategoryName":dict["SubcategoryName"] ?? "",
                                                            "UOMDesc":dict["UOMDesc"] ?? "",
                                                            "UOMID":dict["UOMID"] ?? "",
                                                            "fromCart":false,
                                                            "isAvailable":dict["isAvailable"] ?? "",
                                                            "isPack":dict["isPack"] ?? "",
                                                            "qty":dict["qty"] ?? "",
                                                            "UOMQty":dict["qty"] as? String ?? ""
                                                        ]
                                                        
                                                        singleproduct1.merge(dict: singleproduct2)
                                                        
                                                        productarr.append(singleproduct1)
                                                    }
                                                    
                                                }
                                                
                                                do {

                                                    //Convert to Data
                                                    let jsonData = try JSONSerialization.data(withJSONObject: productarr, options: JSONSerialization.WritingOptions.prettyPrinted)

                                                    //Convert back to string. Usually only do this for debugging
                                                    if let JSONString = String(data: jsonData, encoding: String.Encoding.utf8) {
                                                       print(JSONString)
                                                        json = JSONString
                                                    }

                                                    //In production, you usually want to try and cast as the root data structure. Here we are casting as a dictionary. If the root object is an array cast as [Any].
    //                                                json = try (JSONSerialization.jsonObject(with: jsonData, options: JSONSerialization.ReadingOptions.mutableContainers) as? [String: Any])!


                                                } catch {
                                                    print(error)
                                                }
                                                
                                                
                                                let header = ["User-Id": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                                                                           "X-NAGRIK-LOGIN-TOKEN":UserDefaults.standard.object(forKey: "login_token") ?? "" ,
                                                                           ] as [String : Any]
                                                
                                                 let parameters = [ "SubTotal" : String(subtotal) ,
                                                                    "DeliveryCharge" : String(dc) ,
                                                                    "TotalPrice" : String(total),
                                                                    "zipcode" : txtzipcode.text  ?? "",
                                                                    "address" : textviewaddress.text  ?? "",
                                                                    "Products":json,
                                                                    "Pickup":strradio
                                                  ] as [String : Any]
                                
                                                   callApi(url,
                                                                 method: .post,
                                                                 param: parameters ,
                                                                 extraHeader: header,
                                                                withLoader: true)
                                                         { (result) in
                                                              print("PRODUCTRESPONSE:",result)
                                                             
                                                              if result.getBool(key: "status")
                                                              {
                                                                 let data = result["data"] as? [[String : Any]]
                                                              
                                                                UserDefaults.standard.removeObject(forKey: "items")
                                                                self.productlist = [[String:Any]]()
                                                                GlobalVariables.globalarray = [[String:Any]]()
                                                              //  self.tblproductlist.reloadData()
                                                              //  self.calcamt()
                                                                
                                                                let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "ThnkyouViewController") as! ThnkyouViewController
                                                                      self.navigationController?.pushViewController(nextViewController, animated: true)
                                                              }
                                                           else
                                                           {
                                                               showToast(uiview: self, msg: result.getString(key: "message"))
                                                           }
                                                  }
                                              }
                                              
                                          }
             
}
