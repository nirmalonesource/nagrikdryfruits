//
//  AddPersonViewController.swift
//  SantoshSangal
//
//  Created by My Mac on 02/07/20.
//  Copyright © 2020 My Mac. All rights reserved.
//

import UIKit
import IBAnimatable

class AddPersonViewController: UIViewController {

    @IBOutlet var txtfirstname: AnimatableTextField!
    @IBOutlet var txtlastname: AnimatableTextField!
    @IBOutlet var txtmono: AnimatableTextField!
    @IBOutlet var txtpassword: AnimatableTextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    
    func validation()
               {
                   self.view.endEditing(true)
                   
                    if txtfirstname.text == ""
                   {
                   showToast(uiview: self, msg: "Please enter First Name")
                   }
                   else if txtlastname.text == ""
                   {
                   showToast(uiview: self, msg: "Please enter Last Name")
                   }
                    else if  txtmono.text?.count ?? 0 < 10
                    {
                    showToast(uiview: self, msg: "Please enter valid Mobile Number")
                    }
                   else if txtpassword.text == ""
                   {
                   showToast(uiview: self, msg: "Please enter Password")
                   }
                   else
                   {
                        AddPersonApi()
                   }
               }
    

     // MARK: - Button Action
      
      @IBAction func back_click(_ sender: UIButton) {
          self.navigationController?.popViewController(animated: true)
      }
    
    @IBAction func submit_click(_ sender: UIButton) {
        
        validation()
    }
    
     // MARK: - API
    
    func AddPersonApi()
                  {
                      if !isInternetAvailable(){
                          noInternetConnectionAlert(uiview: self)
                      }
                      else
                      {
                         let url = ServiceList.SERVICE_URL+ServiceList.ADD_SALES_PERSON_API
                        
                        let header = ["User-Id": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                                                   "X-NAGRIK-LOGIN-TOKEN":UserDefaults.standard.object(forKey: "login_token") ?? "" ,
                                                   ] as [String : Any]
                        
                        let parameters = [ "first_name" : txtfirstname.text ?? "" ,
                                           "last_name" : txtlastname.text ?? "",
                                           "mobile_no" : txtmono.text ?? "",
                                           "password":txtpassword.text ?? ""
                          ] as [String : Any]
        
                           callApi(url,
                                         method: .post,
                                         param: parameters ,
                                         extraHeader: header,
                                        withLoader: true)
                                 { (result) in
                                      print("PRODUCTRESPONSE:",result)
                                     
                                      if result.getBool(key: "status")
                                      {
    //                                      let res = result.strippingNulls()
    //                                      let data = res["data"] as? [[String : Any]]
    //                                      self.productlist = data ?? []
    //                                     self.tblproductlist.reloadData()
                                         self.navigationController?.popViewController(animated: true)
                                        
                                      }
                                   else
                                   {
                                       showToast(uiview: self, msg: result.getString(key: "message"))
                                   }
                          }
                      }
                      
                  }
   
}
