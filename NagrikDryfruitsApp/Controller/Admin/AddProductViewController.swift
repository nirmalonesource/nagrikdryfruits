//
//  AddProductViewController.swift
//  SantoshSangal
//
//  Created by My Mac on 30/06/20.
//  Copyright © 2020 My Mac. All rights reserved.
//

import UIKit
import iOSDropDown
import IBAnimatable
import OpalImagePicker
import Photos

class UploadImageCollectionViewCell: UICollectionViewCell {
    
    //ImageView
    @IBOutlet var Img_Vw: UIImageView!
    
    //Button
    @IBOutlet weak var Btn_Close: UIButton!
    
}

class AddProductViewController: UIViewController,UICollectionViewDataSource, UICollectionViewDelegate,UICollectionViewDelegateFlowLayout, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet var txtcategory: DropDown!
    @IBOutlet var txtuom: DropDown!
    @IBOutlet var txtproductname: AnimatableTextField!
    @IBOutlet var txtproductprice: AnimatableTextField!
    @IBOutlet var txtproductdiscprice: AnimatableTextField!
    @IBOutlet var txtproductsize: AnimatableTextField!
    @IBOutlet var swichloose: UISwitch!
    @IBOutlet var switchDOD: UISwitch!
    
    @IBOutlet var Vw_Collection: UICollectionView!
    
    var isedit = false
    
    var editeddictdata = [String:Any]()
    var items = [UIImage]()
    var View = Bool()
    let imagePicker = UIImagePickerController()
    var Categoryid = String()
    var ProductID = ""
    var UOMid = String()
    var isPack = "0"
    var isDealOfDay = "0"
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        if View == false
            {
                items.insert(UIImage(named: "postgreen")!, at: 0)
            }
        
        CategoryApi()
        UOMApi()
        
        txtcategory.didSelect(completion: { (selected, index, id)  in
            print(index,id,selected)
            self.Categoryid = String(id)
              })
        txtuom.didSelect(completion: { (selected, index, id)  in
            print(index,id,selected)
            self.UOMid = String(id)
                     })
        
        if isedit {
            editsetdata()
        }
        
        
    }
    
    func editsetdata()  {
        
        txtcategory.text = editeddictdata["CategoryName"] as? String
         txtuom.text = editeddictdata["UOMDesc"] as? String
         txtproductname.text = editeddictdata["ProductName"] as? String
        txtproductprice.text = editeddictdata["Price"] as? String
        txtproductdiscprice.text = editeddictdata["Dis_price"] as? String
         txtproductsize.text = editeddictdata["PKTSize"] as? String
        
        Categoryid = editeddictdata["CategoryID"] as? String ?? "0"
        UOMid = editeddictdata["UOMID"] as? String ?? "0"
        ProductID = editeddictdata["ProductID"] as? String ?? "0"
        
         let activated = editeddictdata["isPack"] as? String
        
        if activated == "1" {
            swichloose.isOn = true
            isPack = "1"
        }
        
         let isDealOfdy = editeddictdata["isDealOfDay"] as? String
        
        if isDealOfdy == "1" {
            switchDOD.isOn = true
            isDealOfDay = "1"
        }
        
            let string = ("\(editeddictdata["ProductImage"] as? String ?? "")")
         // let ProductImageID = ("\(arrimages[i]["ProductImageID"] as? String ?? "")")
            
            let imageUrl = URL(string: string)
if imageUrl != nil
{
    let imageData = try! Data(contentsOf: imageUrl!)
    let image = UIImage(data: imageData)

    items.append(image!)
        }
        

            
        //  editeditems.append(image!)
       //   itemsID.append(ProductImageID)
            
            print(items)

        
    }
    
    //MARK: -- CollectionView Delegates
       
       // tell the collection view how many cells to make
       func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
           return self.items.count
       }
       
       // make a cell for each cell index path
       func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
           
           // get a reference to our storyboard cell
           let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath as IndexPath) as! UploadImageCollectionViewCell
           
           if indexPath.item != 0 {
               cell.Btn_Close.isHidden = false
               cell.Btn_Close.addTarget(self, action: #selector(AddProductViewController.connected), for: .touchUpInside)
               cell.Btn_Close.tag = indexPath.row
              // cell.Btn_Close.backgroundColor = GlobalColor.SelectedColor
               cell.Img_Vw.image = self.items[indexPath.row]
           }
           else{
               cell.Btn_Close.isHidden = true
               cell.Img_Vw.image = self.items[indexPath.row]
              // cell.Img_Vw.changeImageViewImageColor(color: UIColor.white)
           }
           
           cell.Img_Vw.layer.cornerRadius = cell.Img_Vw.frame.size.height/2
           cell.Img_Vw.layer.masksToBounds = true
           cell.Btn_Close.layer.cornerRadius = cell.Btn_Close.frame.size.height/2
           cell.Btn_Close.layer.masksToBounds = true
        //   cell.Btn_Close.backgroundColor = GlobalColor.SelectedColor
        //   cell.Btn_Close.imageView?.changeImageViewImageColor(color: UIColor.white)
          // cell.Img_Vw.backgroundColor = GlobalColor.SelectedColor
        cell.Img_Vw.layer.borderWidth = 1
        cell.Img_Vw.layer.borderColor = #colorLiteral(red: 0.3450980392, green: 0.6156862745, blue: 0.04705882353, alpha: 1)
           
            self.viewDidLayoutSubviews()
           return cell
       }
       
       @objc func connected(sender: UIButton){
           let buttonTag = sender.tag
           
           items.remove(at: buttonTag)
          
           Vw_Collection.reloadData()
           self.viewDidLayoutSubviews()
       }
       
       //didSelectItemAt
       func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
           // handle tap events
           print("You selected cell #\(indexPath.item)!")
           if 2 - self.items.count != 0 {

                                                   if indexPath.item == 0 {
                                            //            imagePicker.allowsEditing = false
                                            //            imagePicker.sourceType = .photoLibrary
                                            //
                                            //            present(imagePicker, animated: true, completion: nil)
                                                        print("Imageview Clicked")
                                                        let alertViewController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
                                                       
                                                       if let popoverController = alertViewController.popoverPresentationController {
                                                                  popoverController.sourceView = self.view //to set the source of your alert
                                                                  popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0) // you can set this as per your requirement.
                                                                            popoverController.permittedArrowDirections = [] //to hide the arrow of any particular direction
                                                                        }
                                                       
                                                        let camera = UIAlertAction(title: "Take Photo", style: .default, handler: { (alert) in
                                                            self.openCamera()
                                                        })
                                                        let gallery = UIAlertAction(title: "Choose from Gallery", style: .default) { (alert) in
                                                            self.openGallary()
                                                        }
                                                        let cancel = UIAlertAction(title: "Cancel", style: .cancel) { (alert) in
                                                            
                                                        }
                                                        alertViewController.addAction(camera)
                                                        alertViewController.addAction(gallery)
                                                        alertViewController.addAction(cancel)
                                                        self.present(alertViewController, animated: true, completion: nil)
                                                    }
                                        }
                                        else
                                        {
                                            showToast(uiview: self, msg: "You can select only 1 images")
                                        }
    
       }
       
       //didDeselectItemAt
       func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
           let cell = collectionView.cellForItem(at: indexPath)
           cell?.backgroundColor = UIColor.clear
       }
       
       //collectionViewLayout
       func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
           let width = UIScreen.main.bounds.width
           return CGSize(width: (width - 10)/4, height: (width - 10)/4) // width & height are the same to make a square cell
       }
    
    
    //MARK: -- ImageView Delegates
            
            func openCamera() {
                View = true
                guard UIImagePickerController.isSourceTypeAvailable(.camera) else {
                    //Show error to user?
                    popupAlert(title: "Warning", message: "You don't have camera", actionTitles: ["OK"], actions: [{action1 in
                    }, nil])
                    return
                }

                    imagePicker.delegate = self
                    imagePicker.sourceType = UIImagePickerController.SourceType.camera
                    imagePicker.allowsEditing = false
                    self .present(imagePicker, animated: true, completion: nil)
               
               
            }
            
            func openGallary() {
                View = true
    //            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary) {
    //                imagePicker.delegate = self
    //                imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
    //                imagePicker.allowsEditing = false
    //                self.present(imagePicker, animated: true, completion: nil)
    //            }
                
                    let imagePicker = OpalImagePickerController()
                    imagePicker.imagePickerDelegate = self
                imagePicker.allowedMediaTypes = [.image]
                    imagePicker.maximumSelectionsAllowed = 2 - items.count
                    print("REMAINING IMAGE COUNT:",2 - items.count)
                    present(imagePicker, animated: true, completion: nil)
            }
            
            //imagePickerController
     func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any])         {
                // assign pickup image into varibale
                var selectedimage : UIImage!

                if let editedImage = info[.editedImage] as? UIImage
                {
                    selectedimage =  editedImage.fixedOrientation()
                }
                else
                {
                    selectedimage = info[.originalImage] as? UIImage
                    selectedimage =  selectedimage.fixedOrientation()

                }

                View = true

        items.append(selectedimage)
                print(items)

                if items.count != 0 {
                    Vw_Collection.reloadData()
                }

                dismiss(animated: true, completion: nil)
            }

            //imagePickerControllerDidCancel
            public func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
            {
                dismiss(animated: true, completion: nil)
            }
    
    // MARK: - Button Action
    @IBAction func loose_click(_ sender: UISwitch) {
        if sender.isOn {
            isPack = "1"
        }
        else
        {
            isPack = "0"
        }
    }
    @IBAction func dod_click(_ sender: UISwitch) {
        if sender.isOn {
                   isDealOfDay = "1"
               }
               else
               {
                   isDealOfDay = "0"
               }
    }
    
    @IBAction func back_click(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func submit_click(_ sender: UIButton) {
        validation()
        
    }
    
     func validation()
            {
                self.view.endEditing(true)
                if txtcategory.text == ""
                {
                 showToast(uiview: self, msg: "Please Select Category")
                }
                else if txtuom.text == ""
                {
                 showToast(uiview: self, msg: "Please Select Unit Of Mesurement")
                }
                else if txtproductname.text == ""
                {
                showToast(uiview: self, msg: "Please enter Product Name")
                }
                else if txtproductprice.text == ""
                {
                showToast(uiview: self, msg: "Please enter Product Price")
                }
                else if txtproductsize.text == ""
                {
                showToast(uiview: self, msg: "Please enter Product Size")
                }
                else if isDealOfDay == "1" && txtproductdiscprice.text?.trimmingString() == ""
                {
                showToast(uiview: self, msg: "Please enter Product Discount Price")
                }
                else
                {
                     AddProductApi()
                }
            }
    
    // MARK: - API
    
    func AddProductApi()
              {
                  if !isInternetAvailable(){
                      noInternetConnectionAlert(uiview: self)
                  }
                  else
                  {
                     let url = ServiceList.SERVICE_URL+ServiceList.ADD_PRODUCT_API
                    
                    let header = ["User-Id": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                                               "X-NAGRIK-LOGIN-TOKEN":UserDefaults.standard.object(forKey: "login_token") ?? "" ,
                                               ] as [String : Any]
                    
                     let parameters = [
                                        "CategoryID" : Categoryid ,
                                        "ProductName" : txtproductname.text ?? "" ,
                                        "Price" : txtproductprice.text ?? "" ,
                                        "UOMID" : UOMid ,
                                        "PKTSize" : txtproductsize.text ?? "" ,
                                        "isPack" : isPack,
                                        "isDealOfDay" : isDealOfDay,
                                        "Dis_price" : txtproductdiscprice.text ?? "" ,
                                         "ProductID" : ProductID,
                                        // "ItemImage" : ""
                      ] as [String : Any]
                    
                    var imgdata = [Data]()
                                                  if items.count > 1
                                                  {
                                                     
                                                      items.remove(at: 0)
                                                      imgdata = items.compactMap {  $0.jpegData(compressionQuality: 0.4) }
                                                  }
    
                       callApi(url,
                                    method: .post,
                                    param: parameters,
                                    extraHeader: header,
                                    withLoader: true,
                                    data: imgdata,
                                    dataKey: ["ItemImage[]"])
                             { (result) in
                                  print("ADDPRODUCTRESPONSE:",result)
                                 
                                  if result.getBool(key: "status")
                                  {
                                    //  let res = result.strippingNulls()
                                 //     let data = res["data"] as? [[String : Any]]
                               //     showToast(uiview: self, msg: result.getString(key: "message"))
                                     
                                     self.popupAlert(title: "", message: result.getString(key: "message"), actionTitles: ["OK"], actions:[{action1 in
                                     self.navigationController?.popViewController(animated: true)
                                                        }, nil])
                                  }
                               else
                               {
                                   showToast(uiview: self, msg: result.getString(key: "message"))
                               }
                      }
                  }
                  
              }
    
    
      func CategoryApi()
                             {
                                 if !isInternetAvailable(){
                                     noInternetConnectionAlert(uiview: self)
                                 }
                                 else
                                 {
                                    let url = ServiceList.SERVICE_URL+ServiceList.CATEGORY_API
                                   
                                   let header = ["User-Id": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                                                              "X-NAGRIK-LOGIN-TOKEN":UserDefaults.standard.object(forKey: "login_token") ?? "" ,
                                                              ] as [String : Any]
                                   
                                   let parameters = [ "search" : "" ] as [String : Any]
                   
                                      callApi(url,
                                                    method: .post,
                                                    param: parameters ,
                                                    extraHeader: header,
                                                   withLoader: true)
                                            { (result) in
                                                 print("FAVOURITERESPONSE:",result)
                                                
                                                 if result.getBool(key: "status")
                                                 {
                                                    let data = result["data"] as! [[String : Any]]
                                                   
                                                    self.txtcategory.optionArray =  data.compactMap {"\( $0["Name"] as? String ?? "0")"}
                                                     self.txtcategory.optionIds =  data.compactMap {Int( $0["CategoryID"] as? String ?? "0")}
                                                    self.txtcategory.isSearchEnable = false
                                                    
                                                 }
                                              else
                                              {
                                                  showToast(uiview: self, msg: result.getString(key: "message"))
                                              }
                                     }
                                 }
                                 
                             }
    
    func UOMApi()
                                {
                                    if !isInternetAvailable(){
                                        noInternetConnectionAlert(uiview: self)
                                    }
                                    else
                                    {
                                       let url = ServiceList.SERVICE_URL+ServiceList.UOM_API
                                      
                                      let header = ["User-Id": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                                                                 "X-NAGRIK-LOGIN-TOKEN":UserDefaults.standard.object(forKey: "login_token") ?? "" ,
                                                                 ] as [String : Any]
                                      
                                      let parameters = [ "search" : "" ] as [String : Any]
                      
                                         callApi(url,
                                                       method: .get,
                                                       param: parameters ,
                                                       extraHeader: header,
                                                      withLoader: true)
                                               { (result) in
                                                    print("FAVOURITERESPONSE:",result)
                                                   
                                                    if result.getBool(key: "status")
                                                    {
                                                       let data = result["data"] as! [[String : Any]]
                                                       self.txtuom.optionArray =  data.compactMap {"\( $0["UOMDesc"] as? String ?? "0")"}
                                                       self.txtuom.optionIds =  data.compactMap {Int( $0["UOMID"] as? String ?? "0")}
                                                       self.txtuom.isSearchEnable = false
                                                       
                                                    }
                                                 else
                                                 {
                                                     showToast(uiview: self, msg: result.getString(key: "message"))
                                                 }
                                        }
                                    }
                                    
                                }
    
}


extension AddProductViewController: OpalImagePickerControllerDelegate {
   
    func imagePickerDidCancel(_ picker: OpalImagePickerController) {
        //Cancel action?
    }
    
    func imagePicker(_ picker: OpalImagePickerController, didFinishPickingAssets assets: [PHAsset]) {
        //Save Images, update UI
        
        let requestOptions = PHImageRequestOptions()
            requestOptions.resizeMode = PHImageRequestOptionsResizeMode.exact
            requestOptions.deliveryMode = PHImageRequestOptionsDeliveryMode.highQualityFormat
            // this one is key
            requestOptions.isSynchronous = true
            
            for asset in assets
            {
                if (asset.mediaType == PHAssetMediaType.image)
                {

                    PHImageManager.default().requestImage(for: asset , targetSize: PHImageManagerMaximumSize, contentMode: PHImageContentMode.default, options: requestOptions, resultHandler: { (pickedImage, info) in

                        self.items.append(pickedImage!)
                                       print(self.items)
                                       if self.items.count != 0 {
                                           self.Vw_Collection.reloadData()
                                       }// you can get image like this way
                    })

                }
            }
        
        print(items)
        
//        for image in assets {
//            items.append(image.image)
//        }
//        print(items)
//
//        if items.count != 0 {
//            Vw_Collection.reloadData()
//        }
        //Dismiss Controller
        presentedViewController?.dismiss(animated: true, completion: nil)
    }
    
//    func imagePickerNumberOfExternalItems(_ picker: OpalImagePickerController) -> Int {
//        return 1
//    }
//
//    func imagePickerTitleForExternalItems(_ picker: OpalImagePickerController) -> String {
//        return NSLocalizedString("External", comment: "External (title for UISegmentedControl)")
//    }
//
//    func imagePicker(_ picker: OpalImagePickerController, imageURLforExternalItemAtIndex index: Int) -> URL? {
//        return URL(string: "")
//    }
}
