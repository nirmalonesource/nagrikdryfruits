//
//  AdmincategoryViewController.swift
//  NagrikDryfruits
//
//  Created by My Mac on 25/07/20.
//  Copyright © 2020 My Mac. All rights reserved.
//

import UIKit

class AdmincategoryViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {

      @IBOutlet var tbllist: UITableView!
    
      var tablearr = [[String:Any]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
         CategoryApi()
    }
    
     //MARK: - Tableview Data Source
             
             func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
              
                     return tablearr.count
                
             }
             
             func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
              
                
                     let cell = tableView.dequeueReusableCell(withIdentifier: "homecell", for: indexPath) as! homecell
                                tableView.tableFooterView = UIView()
                                cell.lbltitle.text = tablearr[indexPath.row]["Name"] as? String
                                
                                let imgURL = tablearr[indexPath.row]["Image"] as? String ?? ""
                                cell.imgphoto.sd_setImage(with: URL(string:imgURL), placeholderImage: #imageLiteral(resourceName: "Logo"), options:.progressiveLoad, completed: nil)
                    //
                                 self.view.layoutIfNeeded()
                           //     self.viewDidLayoutSubviews()
                    cell.layoutIfNeeded()
                    cell.layoutSubviews()
                    return cell
                
               
                
             }
        
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
                   
            return 150
                   
        }
        
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            
    //            let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "SubCategoryViewController") as! SubCategoryViewController
    //                nextViewController.categorydictdata = tablearr[indexPath.row]
    //                self.navigationController?.pushViewController(nextViewController, animated: true)
                let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "MyProductViewController") as! MyProductViewController
                nextViewController.subcategorydictdata = tablearr[indexPath.row]
                self.navigationController?.pushViewController(nextViewController, animated: true)
        }
        
        @objc func labelAction(gr:UITapGestureRecognizer)
           {
               let searchlbl:UILabel = (gr.view as! UILabel) // Type cast it with the class for which you have added gesture
               print(searchlbl.text ?? "")
              if let url = URL(string: "tel://\(searchlbl.text ?? "")"), UIApplication.shared.canOpenURL(url) {
                   if #available(iOS 10, *) {
                       UIApplication.shared.open(url)
                   } else {
                       UIApplication.shared.openURL(url)
                   }
               }
           }
    

    // MARK: - Button Action
    
    @IBAction func back_click(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
     // MARK: - API
        
          func CategoryApi()
                                 {
                                     if !isInternetAvailable(){
                                         noInternetConnectionAlert(uiview: self)
                                     }
                                     else
                                     {
                                        let url = ServiceList.SERVICE_URL+ServiceList.CATEGORY_API
                                       
                                       let header = ["User-Id": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                                                                  "X-NAGRIK-LOGIN-TOKEN":UserDefaults.standard.object(forKey: "login_token") ?? "" ,
                                                                  ] as [String : Any]
                                       
                                       let parameters = [ : ] as [String : Any]
                       
                                          callApi(url,
                                                        method: .post,
                                                        param: parameters ,
                                                        extraHeader: header,
                                                       withLoader: true)
                                                { (result) in
                                                     print("FAVOURITERESPONSE:",result)
                                                    
                                                     if result.getBool(key: "status")
                                                     {
                                                        let data = result["data"] as? [[String : Any]]
                                                        self.tablearr = data ?? []
                                                       
                                                        // self.tbllist.setContentOffset(.zero, animated: true)
                                                         self.tbllist.setContentOffset(CGPoint(x: 0, y: 10), animated: true)
                                                         self.tbllist.reloadData()
    //                                                    DispatchQueue.main.async {
    //
    //                                                    }
                                                        
                                                     }
                                                  else
                                                  {
                                                      showToast(uiview: self, msg: result.getString(key: "message"))
                                                  }
                                         }
                                     }
                                     
                                 }

}
