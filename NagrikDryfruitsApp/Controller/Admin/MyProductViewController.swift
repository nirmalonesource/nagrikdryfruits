//
//  MyProductViewController.swift
//  SantoshSangal
//
//  Created by My Mac on 26/06/20.
//  Copyright © 2020 My Mac. All rights reserved.
//

import UIKit
import iOSDropDown
import IBAnimatable


class MyProductViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate {
    
    @IBOutlet var lbltitle: UILabel!
    @IBOutlet var btncart: SSBadgeButton!
    @IBOutlet var tblproductlist: UITableView!
     @IBOutlet var txtsearch: UITextField!
    
    var temparr = [String]()
     var productlist = [[String:Any]]()
    var  subcategorydictdata = [String:Any]()
    var quntyarr = (1...50).map{"\($0)"}
    var isfromposter = false
    var FilterProductListArr = [[String:Any]]()
    var SavedProductListArr = [[String:Any]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        lbltitle.text = (subcategorydictdata["Name"] as? String)?.uppercased()
        tblproductlist.tableFooterView = UIView()
       // btncart.badge = String(GlobalVariables.globalarray.count)
      //  btncart.badge = String(99)
      //  btncart.addBadge(number: 99, addedView: btncart)
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
         btncart.badge = String(GlobalVariables.globalarray.count)
        if isfromposter {
                   OfferProductApi()
               }
               else
               {
                   ProductApi()
               }
    }
    
    @IBAction func txtsearchchanged(_ sender: UITextField) {
               
               let  array = SavedProductListArr.filter({
                                // this is where you determine whether to include the specific element, $0
                   ($0["ProductName"]! as AnyObject).localizedCaseInsensitiveContains(sender.text ?? "")
                                // or whatever search method you're using instead
                            })

                            FilterProductListArr = array
                            if FilterProductListArr.count > 0 {
                                productlist = FilterProductListArr
                            }
                            else
                            {
                               if sender.text == "" {
                                   productlist = SavedProductListArr
                               }
                               else
                               {
                                    productlist = FilterProductListArr
                               }
                                
                            }
    //           for index in 0..<productlist.count {
    //               self.temparr[index] = ""
    //               self.pricetemparr[index] = ""
    //           }
               
                            FilterProductListArr = [[:]]
               
                            tblproductlist.reloadData()
           }
   
    
        //MARK: - Tableview Data Source
             
             func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
              
                return productlist.count
                
             }
             
             func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
              
                 let cell = tableView.dequeueReusableCell(withIdentifier: "productcell", for: indexPath) as! productcell
                
                cell.lbltitle.text = productlist[indexPath.row]["ProductName"] as? String
                
               
                cell.lblprice.attributedText = nil
                 cell.lblprice.text = nil
                
                if productlist[indexPath.row]["isDealOfDay"] as? String ?? "" == "1"{
                    
                    
                    
                    cell.lblprice.attributedText = String.makeSlashText("RS \(productlist[indexPath.row]["Dis_price"] as? String ?? "")") //
                    cell.lblofferprice.text = "RS \(productlist[indexPath.row]["Price"] as? String ?? "")"
                }
                else
                {
                    cell.lblofferprice.text = ""
                    cell.lblprice.text = "RS \(productlist[indexPath.row]["Price"] as? String ?? "")" //"RS \(productlist[indexPath.row]["Price"] as? String ?? "")"
                }
                
                
                let imgURL = productlist[indexPath.row]["ProductImage"] as? String ?? ""
                                     cell.imgphoto.sd_setImage(with: URL(string:imgURL), placeholderImage: #imageLiteral(resourceName: "Logo"), options:.progressiveLoad, completed: nil)
                
                cell.btnaddcart.tag = indexPath.row
               
//                cell.txtqty.tag = indexPath.row
//                 cell.txtqty.delegate = self
//                let isIndexValid = temparr.indices.contains(indexPath.row)
//                if isIndexValid {
//                    cell.txtqty.text = temparr[indexPath.row]
//                }
//                else
//                {
//                     cell.txtqty.text = ""
//                }
                
//                cell.btnaddcart.addTarget(self, action:#selector(addcartclick(_:event:)), for: .touchUpInside)
                
                 cell.btnCartAction = { () in
                    
                   // print("QNTY:",cell.txtqty.text ?? "")
                     print("Deletecell:",indexPath.row)
                    
                    self.popupAlert(title: "", message: "Are you sure want to Delete?", actionTitles: ["NO","YES"], actions:[{action1 in

                    },{action2 in
                          self.DeleteProductApi(productid: self.productlist[indexPath.row]["ProductID"] as? String ?? "")
                    }, nil])

                    
                   
                }
                cell.btnedit.imageView?.changeImageViewImageColor(color:#colorLiteral(red: 0.7854140228, green: 0.182675971, blue: 0.153251963, alpha: 1))
                cell.btnEditAction = { () in
                                
                // print("QNTY:",cell.txtqty.text ?? "")
                    print("Editcell:",indexPath.row)
                let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "AddProductViewController") as! AddProductViewController
                nextViewController.editeddictdata = self.productlist[indexPath.row]
                    nextViewController.isedit = true
                self.navigationController?.pushViewController(nextViewController, animated: true)

                }
                
                let Details = productlist[indexPath.row]["Details"] as? [[String:Any]]
                
                if Details?.count ?? 0 > 0 {
                    cell.lbltotalprice.isHidden = false
//                    cell.txtqty.optionArray =  Details!.compactMap {"\( $0["pktsize"] as? String ?? "0") \( $0["uomdesc"] as? String ?? "0")"}
//                     cell.txtqty.isSearchEnable = false
                    
                }
                else
                {
//                    cell.txtqty.optionArray =  quntyarr
//                    cell.txtqty.isSearchEnable = true
                      cell.lbltotalprice.isHidden = true
                }
                
                
              //  cell.txtqty.optionIds =  quntyarr.compactMap {Int( $0["CategoryID"] as? String ?? "0")}
//   cell.txtqty.didSelect(completion: { (selected, index, id)  in
//          print(index,id)
//    print("cell:",indexPath.row)
//    if Details?.count ?? 0 > 0
//    {
//        cell.lbltotalprice.text = "RS \(Details?[index]["price"] as? String ?? "0")"
//    }
//    else
//    {
//    }
//    self.insertElementAtIndex(element: selected, indexx: indexPath.row)
//      })
                 if productlist[indexPath.row]["activated"] as? String ?? "" == "1"{
                    cell.activeswtch.isOn = true
                }
                else
                 {
                    cell.activeswtch.isOn = false
                }
                cell.activeswtch.tag = indexPath.row
             //   cell.activeswtch.addTarget(self, action: Selector(("switchTriggered:")), for: .valueChanged );
                cell.activeswtch.addTarget(self, action:#selector(switchTriggered(sender:)), for: .valueChanged)
                
                 return cell
    }
    
    @objc func switchTriggered(sender: UISwitch) {

        let swtch = sender 
        
        if swtch.isOn {
            ActiveProductApi(productid: productlist[sender.tag]["ProductID"] as? String ?? "", active: "1")
        }
        else
        {
            ActiveProductApi(productid: productlist[sender.tag]["ProductID"] as? String ?? "", active: "0")
        }
        
    }
        
        func insertElementAtIndex(element: String?, indexx: Int) {

            while temparr.count <= indexx {
                temparr.append("")
            }

            temparr.insert(element!, at: indexx)
        }
        
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            
//                     let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "SubCategoryViewController") as! SubCategoryViewController
//                              nextViewController.dictdata = productlist[indexPath.row]
//                    self.navigationController?.pushViewController(nextViewController, animated: true)
          
        }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let aSet = NSCharacterSet(charactersIn:"0123456789").inverted
           let compSepByCharInSet = string.components(separatedBy: aSet)
           let numberFiltered = compSepByCharInSet.joined(separator: "")
        self.insertElementAtIndex(element: textField.text, indexx: textField.tag)
           return string == numberFiltered
    }
    
    
      @objc func addcartclick(_ sender: UIButton, event: Any){
        
    }

    

   // MARK: - Button Action
    
    @IBAction func back_click(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func cart_click(_ sender: UIButton) {
        
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "CartViewController") as! CartViewController
        nextViewController.subcategorydictdata = subcategorydictdata
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    @IBAction func add_click(_ sender: AnimatableButton) {
        
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "AddProductViewController") as! AddProductViewController
             //  nextViewController.subcategorydictdata = subcategorydictdata
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    // MARK: - API
    
    func ActiveProductApi(productid:String,active:String)
                  {
                      if !isInternetAvailable(){
                          noInternetConnectionAlert(uiview: self)
                      }
                      else
                      {
                         let url = ServiceList.SERVICE_URL+ServiceList.ACTIVE_INACTIVE_API
                        
                        let header = ["User-Id": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                                                   "X-NAGRIK-LOGIN-TOKEN":UserDefaults.standard.object(forKey: "login_token") ?? "" ,
                                                   ] as [String : Any]
                        
                         let parameters = [// "category_id" : subcategorydictdata["CategoryID"]! ,
                                         //   "sub_category_id" : subcategorydictdata["SubcategoryID"]! ,
                                            "ProductID" : productid,
                                            "active":active
                          ] as [String : Any]
        
                           callApi(url,
                                         method: .post,
                                         param: parameters ,
                                         extraHeader: header,
                                        withLoader: true)
                                 { (result) in
                                      print("PRODUCTRESPONSE:",result)
                                     
                                      if result.getBool(key: "status")
                                      {
    //                                      let res = result.strippingNulls()
    //                                      let data = res["data"] as? [[String : Any]]
    //                                      self.productlist = data ?? []
    //                                     self.tblproductlist.reloadData()
                                        self.ProductApi()
                                      }
                                   else
                                   {
                                       showToast(uiview: self, msg: result.getString(key: "message"))
                                   }
                          }
                      }
                      
                  }
    
    func DeleteProductApi(productid:String)
              {
                  if !isInternetAvailable(){
                      noInternetConnectionAlert(uiview: self)
                  }
                  else
                  {
                     let url = ServiceList.SERVICE_URL+ServiceList.DELETE_PRODUCT_API
                    
                    let header = ["User-Id": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                                               "X-NAGRIK-LOGIN-TOKEN":UserDefaults.standard.object(forKey: "login_token") ?? "" ,
                                               ] as [String : Any]
                    
                     let parameters = [// "category_id" : subcategorydictdata["CategoryID"]! ,
                                     //   "sub_category_id" : subcategorydictdata["SubcategoryID"]! ,
                                        "ProductID" : productid
                      ] as [String : Any]
    
                       callApi(url,
                                     method: .post,
                                     param: parameters ,
                                     extraHeader: header,
                                    withLoader: true)
                             { (result) in
                                  print("PRODUCTRESPONSE:",result)
                                 
                                  if result.getBool(key: "status")
                                  {
//                                      let res = result.strippingNulls()
//                                      let data = res["data"] as? [[String : Any]]
//                                      self.productlist = data ?? []
//                                     self.tblproductlist.reloadData()
                                    self.ProductApi()
                                  }
                               else
                               {
                                   showToast(uiview: self, msg: result.getString(key: "message"))
                               }
                      }
                  }
                  
              }
       
         func ProductApi()
                                {
                                    if !isInternetAvailable(){
                                        noInternetConnectionAlert(uiview: self)
                                    }
                                    else
                                    {
                                       let url = ServiceList.SERVICE_URL+ServiceList.PRODUCT_API
                                      
                                      let header = ["User-Id": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                                                                 "X-NAGRIK-LOGIN-TOKEN":UserDefaults.standard.object(forKey: "login_token") ?? "" ,
                                                                 ] as [String : Any]
                                      
                                       let parameters = [ "category_id" : subcategorydictdata["CategoryID"]! ,
                                                       //   "sub_category_id" : subcategorydictdata["SubcategoryID"]! ,
                                                          "search" : ""
                                        ] as [String : Any]
                      
                                         callApi(url,
                                                       method: .post,
                                                       param: parameters ,
                                                       extraHeader: header,
                                                      withLoader: true)
                                               { (result) in
                                                    print("PRODUCTRESPONSE:",result)
                                                   
                                                    if result.getBool(key: "status")
                                                    {
                                                        let res = result.strippingNulls()
                                                        let data = res["data"] as? [[String : Any]]
                                                        self.productlist = data ?? []
                                                        self.SavedProductListArr = data ?? []
                                                       self.tblproductlist.reloadData()
                                                    }
                                                 else
                                                 {
                                                     showToast(uiview: self, msg: result.getString(key: "message"))
                                                 }
                                        }
                                    }
                                    
                                }
    
    func OfferProductApi()
                                   {
                                       if !isInternetAvailable(){
                                           noInternetConnectionAlert(uiview: self)
                                       }
                                       else
                                       {
                                          let url = ServiceList.SERVICE_URL+ServiceList.OFFER_PRODUCT_API
                                         
                                         let header = ["User-Id": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                                                                    "X-NAGRIK-LOGIN-TOKEN":UserDefaults.standard.object(forKey: "login_token") ?? "" ,
                                                                    ] as [String : Any]
                                         
                                        let parameters = [:
                                           ] as [String : Any]
                         
                                            callApi(url,
                                                          method: .post,
                                                          param: parameters ,
                                                          extraHeader: header,
                                                         withLoader: true)
                                                  { (result) in
                                                       print("PRODUCTRESPONSE:",result)
                                                      
                                                       if result.getBool(key: "status")
                                                       {
                                                        let res = result.strippingNulls()
                                                        let data = res["data"] as? [[String : Any]]

                                                          //let data = result["data"] as? [[String : Any]]
                                                           self.productlist = data ?? []
                                                          self.SavedProductListArr = data ?? []
                                                          self.tblproductlist.reloadData()
                                                       }
                                                    else
                                                    {
                                                        showToast(uiview: self, msg: result.getString(key: "message"))
                                                    }
                                           }
                                       }
                                       
                                   }
    
   
    
}

